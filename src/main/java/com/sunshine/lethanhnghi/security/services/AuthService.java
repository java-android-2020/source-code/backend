package com.sunshine.lethanhnghi.security.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sunshine.lethanhnghi.dto.request.SignupRequest;
import com.sunshine.lethanhnghi.dto.response.MessageResponse;
import com.sunshine.lethanhnghi.security.jwt.AuthTokenFilter;
import com.sunshine.lethanhnghi.security.jwt.JwtUtils;
import com.sunshine.lethanhnghi.services.EmailService;
import com.sunshine.lethanhnghi.services.OTPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

@Service

public class AuthService {

//    @Autowired
//    private UserRepo userRepository;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private PasswordEncoder passwordEncoder;

//    @Autowired
//    private RoleRepo roleRepository;

    @Autowired
    AuthenticationManager authenticationManager;

//    @Autowired
//    private UserRepo userRepo;

    @Autowired
    private EmailService emailService;

    @Autowired
    private OTPService otpService;

    @Autowired
    AuthTokenFilter authTokenFilter;



    boolean CheckLength(String str){
        return str.length() < 20;
    }

    boolean CheckEmail(String email) {
        String EMAIL_PATTERN = "^[a-z]{1,}([0-9]{0,})+@((vimo)|(mpos)).vn$";
        return Pattern.matches(EMAIL_PATTERN, email) && CheckLength(email);
    }

    boolean CheckPassword(String password) {
        String PASS_PATTERN = "^\\w*(?=\\w{6,})(?=\\w*[0-9])(?=\\w*[a-z])(?=\\w*[A-Z])\\w*$";
        return CheckLength(password) && Pattern.matches(PASS_PATTERN, password);
    }

    public ResponseEntity<MessageResponse> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

        if(!CheckEmail(signUpRequest.getEmail())){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse(HttpStatus.BAD_REQUEST.value(), "Error: Email Format!"));
        }

        if(!CheckPassword(signUpRequest.getPassword())){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse(HttpStatus.BAD_REQUEST.value(), "Error: Password Format!"));
        }
        try {

            if (userRepository.existsByEmail(signUpRequest.getEmail())
                    && (userRepository.findByEmail(signUpRequest.getEmail()).get().getAccStatus()).equals("Acc_Active")) {
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse(HttpStatus.BAD_REQUEST.value(), "Error: Email is already in use!"));
            }
            if (userRepository.existsByEmail(signUpRequest.getEmail())
                    && !(userRepository.findByEmail(signUpRequest.getEmail()).get().getAccStatus()).equals(EaccountStatus.Acc_Active.toString())) {
                userRepository.findByEmail(signUpRequest.getEmail()).get().setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
                userRepository.save(userRepository.findByEmail(signUpRequest.getEmail()).get());
            }else{
                User user = new User(
                        signUpRequest.getEmail(),
                        passwordEncoder.encode(signUpRequest.getPassword())
                );

                Set<Role> roles = new HashSet<>();
                user.setRoles(roles);
                Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
                user.setRoles(roles);
                //set default account status
                user.setAccStatus(EaccountStatus.Acc_NotActive.toString());
                userRepository.save(user);
            }


            Long lastTryTime = otpService.getLastTryTime(signUpRequest.getEmail());
            if(lastTryTime != null && lastTryTime >= System.currentTimeMillis())// && lastTryTime > 1000L
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse(400,"wait15m!"));

            int otp = otpService.generateOTP(signUpRequest.getEmail());
            emailService.sendOtpMessage(signUpRequest.getEmail(), "Risk Verifier OTP Code"
                    ,"<h3>your OTP code is : </h3><h1>" + otp + "</h1>");
            System.out.println("OTP is : " + otp);


            return ResponseEntity
                    .ok()
                    .body(new MessageResponse(HttpStatus.OK.value(),"Success"));
        }catch (Exception ex){
            ex.printStackTrace();
            MessageResponse message = new MessageResponse(HttpStatus.BAD_REQUEST.value(), "Something wrong");
            return ResponseEntity
                    .badRequest()
                    .body(message);
        }
    }


    public ResponseEntity<?> login(LoginRequest loginRequest){
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

            //check active acc
            Optional<User> user = userRepo.findByEmail(loginRequest.getEmail());
            if(user.isPresent()){
                User thisUser;
                thisUser = user.get();
                if(thisUser.getAccStatus().equals(EaccountStatus.Acc_NotActive.toString())) {
                    return new ResponseEntity<>(new MessageResponse(401,"Account not verified!"),HttpStatus.UNAUTHORIZED);
                }
                if(thisUser.getAccStatus().equals(EaccountStatus.Acc_Pending_ActiveByAdmin.toString())) {
                    return new ResponseEntity<>(new MessageResponse(401,"Account not active by admin!"),HttpStatus.UNAUTHORIZED);
                }
            }

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

            return ResponseEntity.ok(new JwtResponse(jwt, "Bearer", userDetails.getEmail()));
        }catch(AuthenticationException e){
            return new ResponseEntity<>(new MessageResponse(401,"Email or password not correct!"),HttpStatus.UNAUTHORIZED);
        }
    }


    public ResponseEntity<?> verifyOTP(OtpRequest otpRequest){
        if(otpRequest.getOtp() >= 100000){
            Long lastTryTime = otpService.getLastTryTime(otpRequest.getEmail());
            if(lastTryTime != null && lastTryTime >= System.currentTimeMillis())
                return new ResponseEntity<>(new MessageResponse(400,"wait15m!"),HttpStatus.BAD_REQUEST);
            if(otpService.getOtp(otpRequest.getEmail()) == otpRequest.getOtp()){
                otpService.clearOTP(otpRequest.getEmail());
                otpService.clearTryTime(otpRequest.getEmail());
                userRepo.findByEmail(otpRequest.getEmail()).get()
                        .setAccStatus(EaccountStatus.Acc_Pending_ActiveByAdmin.toString());
                userRepo.save(userRepo.findByEmail(otpRequest.getEmail()).get());
                return new ResponseEntity<>(new MessageResponse(200,"SUCCESS!"),HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(new MessageResponse(400,"INCORRECT!"),HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<MessageResponse> updatePass(String updatePass, HttpServletRequest req) throws JsonProcessingException {
        JsonNode riskNode = new ObjectMapper().readTree(updatePass);
        if(passwordEncoder.matches(riskNode.get("oldPass").asText(), authTokenFilter.whoami(req).get().getPassword())){
            if(CheckPassword(riskNode.get("newPass").asText())==false){
                return ResponseEntity
                        .badRequest()
                        .body(new MessageResponse(HttpStatus.BAD_REQUEST.value(), "Error: Password Format!"));
            }else {
                Optional<User> user = authTokenFilter.whoami(req);
                user.get().setPassword(passwordEncoder.encode(riskNode.get("newPass").asText()));
                userRepo.save(user.get());

                MessageResponse message = new MessageResponse(HttpStatus.OK.value(),"Success");
                return ResponseEntity
                        .ok()
                        .body(message);
            }
        }else{
            MessageResponse message = new MessageResponse(HttpStatus.OK.value(),"Error: Wrong password!");
            return ResponseEntity
                    .ok()
                    .body(message);
        }

    }

    public void sendOTP(String email) throws JsonProcessingException {
        JsonNode riskNode = new ObjectMapper().readTree(email);
        try {
            int otp = otpService.generateOTP(riskNode.get("email").asText());
            emailService.sendOtpMessage(riskNode.get("email").asText(), "Risk Verifier OTP Code"
                    ,"<h3>your OTP code is : </h3><h1>" + otp + "</h1>");
            System.out.println("OTP is : " + otp);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }


    public ResponseEntity<MessageResponse> resetPass(ResetPasswordRequest resetPass){
        if(resetPass.getOtp() >= 100000){
            if (CheckPassword(resetPass.getPass()) == false)
                return new ResponseEntity<>(new MessageResponse(400,"Error: Password Format!"),HttpStatus.BAD_REQUEST);

            Long lastTryTime = otpService.getLastTryTime(resetPass.getEmail());
            if(lastTryTime != null && lastTryTime >= System.currentTimeMillis())
                return new ResponseEntity<>(new MessageResponse(400,"wait15m!"),HttpStatus.BAD_REQUEST);
            if(otpService.getOtp(resetPass.getEmail()) == resetPass.getOtp()){
                otpService.clearOTP(resetPass.getEmail());
                otpService.clearTryTime(resetPass.getEmail());
                Optional<User> user = userRepo.findByEmail(resetPass.getEmail());
                    user.get().setPassword(passwordEncoder.encode(resetPass.getPass()));
                    userRepo.save(user.get());
                    otpService.clearOTP(resetPass.getEmail());
                    return new ResponseEntity<>(new MessageResponse(200,"Success"),HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(new MessageResponse(400,"Error: Wrong otp!"),HttpStatus.BAD_REQUEST);
    }
}
