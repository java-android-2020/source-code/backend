package com.sunshine.lethanhnghi.repository;

import org.springframework.data.repository.CrudRepository;

public interface CustomerRepo extends CrudRepository<User,Long> {
}
