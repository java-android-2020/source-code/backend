package com.sunshine.lethanhnghi.services;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
public class OTPService {



    private static final Integer EXPIRE_MINS = 2;
    private LoadingCache<String, Integer> otpCache;
    private HashMap<String, Long> attempt = new HashMap<String, Long>();
    public OTPService(){
        super();
        otpCache = CacheBuilder.newBuilder().
                expireAfterWrite(EXPIRE_MINS, TimeUnit.MINUTES)
                .build(new CacheLoader<String, Integer>() {
                    public Integer load(String key) {
                        return 0;
                    }
                });
    }
    //This method is used to push the opt number against Key. Rewrite the OTP if it exists
    //Using user id  as key
    public int generateOTP(String key) {
            Random random = new Random();
            int otp = 100000 + random.nextInt(900000);
            otpCache.put(key, otp);
            Long tryTimeVal = attempt.get(key);
            if(tryTimeVal == null || tryTimeVal > 1000L)
                attempt.put(key,0L);
            return otp;
    }
    //This method is used to return the OPT number against Key->Key values is username
    public int getOtp(String key){
        try{
            if(attempt.get(key)>=4) {
                Long timeSt = System.currentTimeMillis()+120000L;//15min 900000L
                attempt.replace(key,timeSt);
                clearOTP(key);
            }else {
                attempt.replace(key,attempt.get(key)+1);
            }
            System.out.println(attempt);
            return otpCache.get(key);
        }catch (Exception e){
            return -1;
        }
    }
    //This method is used to clear the OTP catched already
    public void clearOTP(String key){
        otpCache.invalidate(key);
    }

    //veryfied time
    public Long getLastTryTime(String key){
        try {
            return attempt.get(key);
        }catch (Exception e){
            return 0L;
        }
    }

    public void clearTryTime(String key){
        attempt.remove(key);
        attempt.entrySet().removeIf(e -> (e.getValue() < System.currentTimeMillis()
                && getOtp(e.getKey()) >= 100000));
    }
}
