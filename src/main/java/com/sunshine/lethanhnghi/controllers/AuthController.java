package com.sunshine.lethanhnghi.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sunshine.lethanhnghi.dto.request.LoginRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {


//	@Autowired
//	AuthService authService;
//
//	@Autowired
//	AuthTokenFilter authTokenFilter;
//
//	@Autowired
//	PasswordEncoder encoder;
//
//	@Autowired
//	JwtUtils jwtUtils;
//
//	@Autowired
//	UserRepo userRepo;
//
//	@Autowired
//	private OTPService otpService;

	@Autowired
	PasswordEncoder passwordEncoder;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		return authService.login(loginRequest);
	}

	@PostMapping("/signup")
	public ResponseEntity<MessageResponse> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		return authService.registerUser(signUpRequest);

	}

	@PostMapping("/verifyOTP")
	public ResponseEntity<?> verifyOTP(@RequestBody OtpRequest otpRequest){
		return authService.verifyOTP(otpRequest);
	}

	@PutMapping("/updatePass")
	public ResponseEntity<MessageResponse> updatePass(@RequestBody String updatePass, HttpServletRequest req) throws JsonProcessingException {
		return authService.updatePass(updatePass, req);
	}

	@PostMapping("/forgotPass")
	public void forgetMe(@RequestBody String email) throws JsonProcessingException {
		authService.sendOTP(email);
	}

	@PostMapping("/verifyOTPPass")
	public ResponseEntity<MessageResponse> OTPfogotPass(@RequestBody ResetPasswordRequest resetPass) {
		return authService.resetPass(resetPass);
	}

}
