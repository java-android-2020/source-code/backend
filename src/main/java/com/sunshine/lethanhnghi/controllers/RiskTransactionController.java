//package com.sunshine.lethanhnghi.controllers;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.nextpay.risk_management.model.RiskTransaction;
//import com.nextpay.risk_management.repository.RiskTransRepo;
//import com.nextpay.risk_management.services.RiskTransService;
//import org.modelmapper.ModelMapper;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@CrossOrigin(origins = "*", maxAge = 3600)
//@RestController
//@RequestMapping("api/risktransaction")
//public class RiskTransactionController {
//
//    //final
//    @Autowired
//    RiskTransRepo riskTransRepo;
//
//    @Autowired
//    ModelMapper modelMapper;
//
//    @Autowired
//    RiskTransService riskTransService;
//
//
//    @GetMapping("/getRisk")
//    public List<RiskTransaction> getList(@RequestParam("code") String code, @RequestParam("date") String date,
//                                         @RequestParam("handler") String handler,
//                                         @RequestParam("page") int page, @RequestParam("item") int item) {
//
//        if (code.equals("ALL")) code = "";
//        if (date.equals("NODATE")) date="";
//        if (handler.equals("REMAIN")){
//            return riskTransService.findAllRemain(code, date, page, item);
//        }
//        else {
//            return riskTransService.findAll(code, date, page, item);
//        }
//
//    }
//
//
//    @PutMapping("/updateStatus")
//    public ResponseEntity<?> updateTransacStatus(@RequestBody String message) throws JsonProcessingException {
//        return riskTransService.updateStatus(message);
//    }
//
//
//}
