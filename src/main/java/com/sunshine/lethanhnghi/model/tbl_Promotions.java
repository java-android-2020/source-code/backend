package com.sunshine.lethanhnghi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_Promotions")
public class tbl_Promotions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "promotionsid")
    private Long promotionsid;

    @Column(name = "promotionstitel")
    private String promotionstitel;

    @Column(name = "promotionsditail")
    private String promotionsditail;

    @Column(name = "promotionsimage", length = 1000)
    private byte[] promotionsimage;

    @Column(name = "promotionsstatus")
    private int promotionsstatus;

    @Column(name = "created")
    @CreatedDate
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date created;



}
