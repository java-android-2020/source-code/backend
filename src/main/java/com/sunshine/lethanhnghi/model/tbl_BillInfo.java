package com.sunshine.lethanhnghi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_BILLINFO")
public class tbl_BillInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "billid")
    private String billid;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "foodid")
    private Long foodid;
}
