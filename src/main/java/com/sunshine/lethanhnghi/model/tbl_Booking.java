package com.sunshine.lethanhnghi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_booking")
public class tbl_Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "bookingid")
    private String bookingid;

    @Column(name = "email")
    private String email;

    @Column(name = "bookingtime") //thoi gian dat ban
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date bookingtime;

    @Column(name = "totalseats") //tổng số chỗ ngồi
    private int totalseats;

    @Column(name = "depositid") //đặt cọc
    private Long depositid;

    @Column(name = "bookingstatus") //dùng để xe boking đã thnh toán chưa: 0: là chưa thanh toán , 1:đã thanh toán
    private int bookingstatus;




}
