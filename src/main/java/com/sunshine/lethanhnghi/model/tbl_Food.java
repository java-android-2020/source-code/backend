package com.sunshine.lethanhnghi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_FOOD")
public class tbl_Food {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "foodid")
    private Long foodid;

    @Column(name = "foodname")
    private String foodname;

    @Column(name = "foodprice")
    private Long foodprice;

    @Column(name = "created")
    @CreatedDate
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date created;

    @Column(name = "foodstatus")
    private int foodstatus;


//
//    public tbl_Food(String foodName, String type, byte[] picByte) {
//        FoodName = foodName;
//        Type = type;
//        this.picByte = picByte;
//    }
}
