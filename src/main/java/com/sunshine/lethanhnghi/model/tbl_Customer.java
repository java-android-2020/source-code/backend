package com.sunshine.lethanhnghi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_Customer")

public class tbl_Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "phonenumber")
    private String phonenumber;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "password")
    private String password;

    @Column(name = "totalmoney")
    private Long totalmoney;

    public tbl_Customer(String email, String phonenumber, String fullname, String password, Long totalmoney) {
        this.email = email;
        this.phonenumber = phonenumber;
        this.fullname = fullname;
        this.password = password;
        this.totalmoney = totalmoney;
    }

    public tbl_Customer(String email, String phonenumber, String fullname, String password) {
        this.email = email;
        this.phonenumber = phonenumber;
        this.fullname = fullname;
        this.password = password;
    }

}
