package com.sunshine.lethanhnghi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_bankaccount")
public class tbl_BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "accountno")
    private String accountno;

    @Column(name = "balance")
    private Long balance;

    @Column(name = "status")
    private int status;

    @Column(name = "email")
    private String email;


}
