package com.sunshine.lethanhnghi.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_BILL")
public class tbl_Bill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "billid")
    private String billid;

    @Column(name = "pointid")
    private Long pointid;

    @Column(name = "bookingid")
    private String bookingid;

    @Column(name = "discount")
    private Long discount;

    @Column(name = "billstatus")
    private int billstatus;

    @Column(name = "paydate")
    @CreatedDate
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date paydate;
}
