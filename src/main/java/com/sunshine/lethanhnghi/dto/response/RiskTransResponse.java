package com.sunshine.lethanhnghi.dto.response;


import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class RiskTransResponse implements Serializable {

    @Id
    private Long id;

    private String productCode;

    private String productName;

    private Timestamp created;
}
